import express from "express";
import { ApiRouter } from "./api.router";

export class AutenticacaoApi extends ApiRouter {
  private readonly path: string;

  constructor() {
    super();
    this.path = "/autenticacao";
  }

  public active(): boolean {
    return true;
  }

  public async applyRoutes(server: express.Application): Promise<void> {}
}
