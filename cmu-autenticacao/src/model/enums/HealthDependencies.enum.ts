export enum HealthDependenciesEnum {
  OK = "ok",
  INDISPONÍVEL = "indisponível",
  INATIVO = "inativo",
  EM_IMPLEMENTACAO = "Em Implementação",
}
