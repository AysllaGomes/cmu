const express = require("express");
const monitor = require("@labbsr0x/express-monitor");
const nockWappa = require("../../src/nocks/NockWappa");
const nockWappaGestor = require("../../src/nocks/NockWappaGestor");

process.env.NODE_ENV = "test";

monitor.Monitor.init(express(), true);
nockWappa.executaNocksWappa();
nockWappaGestor.executaNocksWappaGestor();
