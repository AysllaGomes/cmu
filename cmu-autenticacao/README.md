# cmu-autenticacao

1. [Descrição](#1-descrição)
2. [Início rápido](#2-início-rápido)
3. [Componentes](#3-componentes)
4. [Testes](#4-testes)


## 1. Descrição

O cmu-autenticacao é um microsserviço que visa prover ...

Este projeto é codificado em [TypeScript](<https://www.typescriptlang.org/docs/home.html>) e utiliza as tecnologias [NodeJS](<https://pt.wikipedia.org/wiki/Node.js>) e [Express](https://expressjs.com/pt-br/).


## 2. Início rápido

Para rodar o projeto, execute o comando:

``` bash
npm run docker
```

Para checar o status da execução, utilize o comando:

``` bash
docker-compose ps
```

## 3. Componentes

Este microsserviço possui a seguinte estrutura de componentes:

| Componente                 | Descrição             | Endpoint                | Saiba mais em |
| :---                   | :----:                | :----:                   | ---:         |
| **_Info_**                   | Obtém as informações sobre versão da aplicação, commit e dependências            | <http://localhost:3000/info>             | [info](#31-info) |
| **_Documentação das APIs_**  | Realiza a documentação automática das apis e endpoints        | <http://localhost:3000/api-docs> | [api-docs](#32-documentação-da-api)     |
| **_Logs_**           | Registra os eventos no console.        | <http://localhost:5601> | [logs](#33-logs-com-efk-elasticsearch-fluentd-kibana)     |
| **_Métricas_**               | Métricas expostas pela aplicação, Prometheus e Grafana (monitoração)                 | <http://localhost:3000/metrics>, <http://localhost:9090/> e <http://localhost:3100/> | [metrics](#34-métricas)       |
| **_Rastreamento_**           | Realiza o rastreamento da aplicação           | <http://localhost:16686> | [tracing](#35-tracing)   |
| **_Saúde da aplicação_**     | Mede a saúde da aplicação para o Kubernets (monitoração)           | <http://localhost:3000/health> e <http://localhost:3000/ready> | [health](#36-health-e-healthcheck)       |
| **_Saúde das dependências_** | Mede a saúde das dependências da aplicação (monitoração)           | <http://localhost:3000/healthcheck> | [healthcheck](#36-health-e-healthcheck)       |


### 3.1. Info

A fim de expor as váriaveis de ambiente, versão da aplicação e outras informações deste projeto, foi configurado o seguinte endpoint: <http://localhost:3000/info>


### 3.2. Documentação da API

Para acessar a documentação da API no Swagger UI, acesse o endpoint: <http://localhost:3000/api-docs>

A documentação deve ser escrita no padrão [openApi](https://swagger.io/specification/).
Como as bibliotecas usadas ja definem a estrutura base da documentação sera necessario realizar a documentação das rotas e dos modelos nos arquivos presentes nas pastas model e routes.
Para as rotas deve se usar o Path Item Object, ja nos modelos deve se usar o Definitions.

Path Item Object

```ts
/**
 * @swagger
 * /errors:
 *   get:
 *     description: List of errors
 *     summary: List of errors that application can throw.
 *     tags:
 *       - Errors
 *     responses:
 *       200:
 *         description: Errors
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Error'
 */
```

Definitions

```ts
/**
 * @swagger
 *
 * definitions:
 *   Error:
 *     type: object
 *     properties:
 *       errorCode:
 *         type: integer
 *       errorMessage:
 *         type: string
 *     example:
 *       errorCode: 1404
 *       errorMessage: "Nenhum Registro Encontrado"
 */
```


### 3.3. Logs com EFK (Elasticsearch + Fluentd + Kibana)

Coleta de logs pelo [Fluentd](<https://www.fluentd.org/>), armazenamento no [Elasticsearch](<https://www.elastic.co/pt/products/elasticsearch>) e visualização no [Kibana](<https://www.elastic.co/pt/products/kibana>).

1. Para acessar o endpoint de visualização de logs (Kibana), clique em <http://localhost:5601>
2. No campo **index pattern** digite * e no combo **Time-field name** selecione @timestamp, depois clique em Create.
3. Use a opção **Discover** para explorar o log.


### 3.4. Métricas

Este projeto expõe métricas no endpoint: <http://localhost:3000/metrics>

Para monitorar as métricas são utilizadas as ferramentas [Prometheus](<https://prometheus.io/docs/introduction/overview/>) e [Grafana](<http://docs.grafana.org/>), expostos nos endpoints: <http://localhost:9090/> e <http://localhost:3100/>, respectivamente.

Para conhecer mais sobre "Os 4 Sinais de Ouro" ("The Four Golden Signals") da monitoração, clique [aqui](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/).


### 3.5. Tracing

Para realizar o tracing este projeto utiliza uma implementação da ferramenta [Jaeger](<https://www.jaegertracing.io/>) chamada [jaeger-tracer-decorator](https://github.com/CarlosPanarello/jaeger-tracer-decorator).

A interface do Jaeger é exposta no endpoint: <http://localhost:16686>


### 3.6. Health e Healthcheck

Para que o [Kubernetes](<https://kubernetes.io/pt/>) possa monitorar a saúde da aplicação, foram incluidos três endpoints (/health, /healthcheck e /ready) com os seguintes objetivos:

1. [/health:](<http://localhost:3000/health>) testar quando a aplicação está "viva";
2. [/healthcheck:](<http://localhost:3000/healthcheck>) testar se as dependencias externas da aplicação estão respondendo;
3. [/ready:](<http://localhost:3000/ready>) testar se a aplicação está pronta para atender.

**Atenção:** o **/health** deve ser o mais simples possível de forma a não comprometer a monitoração pelo Kubernetes.


## 4. Testes

### 4.1 Testes unitários

Neste projeto foram incluídos exemplos de testes unitários implementados em [Jest](<https://jestjs.io/docs/en/getting-started>).

Para editar os testes unitários, edite os scripts do diretório: **/src/test/unit**.

Para executar os testes unitários, execute o comando:

``` bash
npm test
```
